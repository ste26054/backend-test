module HubspotHandler
  class HubspotService
    def initialize(hapikey: ENV['HAPIKEY'], **args)
      @client = ::HubspotApi::Client.new(hapikey: hapikey)
    end

    def call
      perform
    end

    protected

    attr_reader :client

    private

    def perform; end
  end
end
