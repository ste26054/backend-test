module HubspotHandler
  class LinkUserService < HubspotService
    def initialize(user:, **args)
      super
      @user = user
    end

    private

    attr_reader :user, :contact

    def perform
      create_contact

      OpenStruct.new(success?: true)
    rescue Hubspot::RequestError => exception
      post_update_user

      OpenStruct.new(
        success?: false,
        error: exception.response&.parsed_response.try(:[], 'message')
      )
    end

    def create_contact
      return if user.hubspot_linked?

      @contact = client.create_contact!(email: user.email)
      post_update_user
    end

    def post_update_user
      if contact
        user.hubspot_link_status = :created
        user.hubspot_id = contact.vid
      else
        user.hubspot_link_status = :failed
      end

      user.save
    end
  end
end