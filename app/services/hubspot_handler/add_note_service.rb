module HubspotHandler
  class AddNoteService < HubspotService
    def initialize(user:, note:, **args)
      super
      @user = user
      @note = note
    end

    private

    attr_reader :user, :note

    def perform
      create_and_associate_note

      OpenStruct.new(success?: true)
    rescue Hubspot::RequestError => exception
      OpenStruct.new(
        success?: false,
        error: exception.response&.parsed_response.try(:[], 'message')
      )
    end

    def create_and_associate_note
      engagement_note = client.create_engagement_note!(note: note)

      client.associate_note!(
        contact_id: user.hubspot_id,
        engagement_note_id: engagement_note.id
      )
    end
  end
end
