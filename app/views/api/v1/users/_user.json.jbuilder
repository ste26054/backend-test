json.extract! user, :id, :email, :hubspot_link_status, :hubspot_id, :created_at, :updated_at
json.url api_v1_user_url(user, format: :json)
