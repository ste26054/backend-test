class LinkUserJob < ApplicationJob
  queue_as :default

  def perform(user:)
    #TODO: raise an exception if service call returns an error so it's caught by the job processor (e.g. Sidekiq)
    HubspotHandler::LinkUserService.new(user: user).call
  end
end