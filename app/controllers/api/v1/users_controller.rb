class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :link_to_hubspot, :add_note]

  # TODO: Add an authorization layer

  # GET /users.json
  def index
    @users = User.all
    render json: @users
  end

  # GET /users/1.json
  def show
    render json: @user
  end

  # POST /users.json
  # TODO: If user creation process becomes more complex, move its logic into a "UserCreationService"
  def create
    @user = User.new(user_params)

    if @user.save
      LinkUserJob.perform_later(user: @user) unless Rails.env.test?

      render :show, status: :created, location: api_v1_user_path(@user)
    else
      render json: { error: @user.errors }, status: :unprocessable_entity
    end
  end

  # POST /users/1/link_to_hubspot.json
  def link_to_hubspot
    result = HubspotHandler::LinkUserService.new(user: @user).call

    if result.success?
      @user.reload
      render :show, status: :ok, location: api_v1_user_path(@user)
    else
      render json: { error: result.error }, status: :unprocessable_entity
    end
  end

  # POST /users/1/add_note.json
  def add_note
    note = params.require(:note)
    result = HubspotHandler::AddNoteService.new(user: @user, note: note).call

    if result.success?
      render :show, status: :created, location: api_v1_user_path(@user)
    else
      render json: { error: result.error }, status: :unprocessable_entity
    end
  rescue ActionController::ParameterMissing => ex
    render json: { error: ex.message }, status: :unprocessable_entity
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  rescue ActiveRecord::RecordNotFound => ex
    render json: { error: ex.message }, status: :not_found
    return
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email)
  end
end
