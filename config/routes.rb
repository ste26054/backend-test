Rails.application.routes.draw do

  root 'welcome#index'

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: [:create, :index, :show] do
        member do
          post :link_to_hubspot
          post :add_note
        end
      end
    end
  end
end
