module HubspotApi
  class Client

    def initialize(hapikey: ENV['HAPIKEY'])
      raise Hubspot::ConfigurationError if hapikey.blank?

      Hubspot.configure(hapikey: hapikey)
    end

    def create_contact!(email: '', params: {})
      Hubspot::Contact.create!(email, params)
    end

    def create_engagement_note!(note: '')
      Hubspot::EngagementNote.create!(nil, note)
    end

    def associate_note!(contact_id:, engagement_note_id:)
      Hubspot::Engagement.associate!(engagement_note_id, 'contact', contact_id)
    end
  end
end