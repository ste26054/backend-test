require 'rails_helper'

RSpec.describe HubspotHandler::AddNoteService do
    let(:user) { create(:user, email: 'a_new_user_3@domain.com') }
    let(:link_user_service) { HubspotHandler::LinkUserService.new(user: user) }
    let(:add_note_result) { HubspotHandler::AddNoteService.new(user: user, note: 'TEST NOTE').call }

    context 'with a linked user' do
      it 'links a given note' do
        VCR.use_cassette("services/hubspot_handler/add_note_user") do
          link_user_service.call
          
          expect(add_note_result).to be_an_instance_of(OpenStruct)
          expect(add_note_result.success?).to eq(true)
        end
      end
    end

end