require 'rails_helper'

RSpec.describe HubspotHandler::LinkUserService do
  context 'when user is new' do
    let(:user) { create(:user, email: 'a_new_user@domain.com') }
    let(:result) { HubspotHandler::LinkUserService.new(user: user).call }

    it 'links the user and update its attributes accordingly' do
      VCR.use_cassette("services/hubspot_handler/link_new_user") do
        expect(result).to be_an_instance_of(OpenStruct)
        expect(result.success?).to eq(true)
        
        user.reload

        expect(user.hubspot_link_status).to eq('created')
        expect(user.hubspot_id).to_not be_nil
      end
    end
  end

  context 'when there is an api error' do
    let(:email) { 'a_new_user_2@domain.com' }
    let(:user) { create(:user, email: 'a_new_user@domain.com') }
    let(:result) { HubspotHandler::LinkUserService.new(user: user, hapikey: 'incorrect_key').call }

    it 'returns an openstruct with an error and marks sets the hubspot_link_status to "failed"' do
      VCR.use_cassette("services/hubspot_handler/link_user_error") do
        expect(result).to be_an_instance_of(OpenStruct)
        expect(result.success?).to eq(false)
        expect(result.error).to_not be_blank

        user.reload

        expect(user.hubspot_link_status).to eq('failed')
        expect(user.hubspot_id).to be_nil
      end
    end
  end
end