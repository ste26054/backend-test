require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:email) }

    it { should validate_uniqueness_of(:email) }
    
    it do
      should define_enum_for(:hubspot_link_status).
        with_values(pending: 0, created: 1, failed: 2)
    end

    it { should allow_value("email@domain.con").for(:email) }
    
    it { should_not allow_value("wrong_email").for(:email) }
  end

  describe 'creation' do
    let(:user) { create(:user) }

    it do
      expect(user.hubspot_link_status).to eq('pending')
      expect(user.hubspot_id).to be_nil
    end
  end
end
