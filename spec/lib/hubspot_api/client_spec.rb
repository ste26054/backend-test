require 'rails_helper'

RSpec.describe HubspotApi::Client do
  let(:client) { HubspotApi::Client.new }
  
  describe '#initialize' do
    context 'when api key is not provided' do
      it 'raises an error' do
        expect { HubspotApi::Client.new(hapikey: nil) }.to raise_error(HubSpot::ConfigurationError)
      end
    end
  end

  describe '.create_contact!' do

    context "when the contact does not exist" do

      it "creates the contact" do
        VCR.use_cassette("lib/hubspot_api/create_contact") do
          email = "newcontact#{Time.now.to_i}@test.com"
          contact = client.create_contact!(email: email)

          expect(contact).to be_an_instance_of(Hubspot::Contact)
          expect(contact.email).to match(/newcontact.*@test.com/)
        end
      end
    end

    context 'when the contact already exists' do

      let(:email) { "a_new_contact@test.com" }

      it 'returns an error' do
        VCR.use_cassette("lib/hubspot_api/create_duplicate_contact") do
          client.create_contact!(email: email)
          expect { client.create_contact!(email: email) }.to raise_error(Hubspot::RequestError)

        end
      end
    end
  end

  describe '.create_engagement_note!' do

    let(:body) { 'TEST NOTE' }
    let(:engagement_note) { client.create_engagement_note!(note: body) }

    it 'creates an engagement note' do
      VCR.use_cassette("lib/hubspot_api/create_engagement_note") do        
        expect(engagement_note).to be_an_instance_of(Hubspot::EngagementNote)
        expect(engagement_note.body).to eq(body)
      end
    end
  end

  describe '.associate_note!' do

    let(:contact) { client.create_contact!(email: "another_contact@domain.com") }
    let(:engagement_note) { client.create_engagement_note!(note: 'TEST NOTE') }

    context 'when contact and note exists' do
      it 'associates the note with the contact' do
        VCR.use_cassette("lib/hubspot_api/associate_note") do        
          expect(
            client
            .associate_note!(
              contact_id: contact.vid,
              engagement_note_id: engagement_note.id
            )
          ).to eq(nil)
        end
      end
    end

    context 'when contact or note are invalid' do
      it 'raises an error' do
        VCR.use_cassette("lib/hubspot_api/associate_note_invalid") do        
          expect do
            client
            .associate_note!(
              contact_id: -1,
              engagement_note_id: -1
            )
          end.to raise_error(Hubspot::RequestError)
        end
      end
    end
  end



end