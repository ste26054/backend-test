require 'swagger_helper'

describe 'Users API' do
  path '/api/v1/users.json' do
    get 'Lists all the users' do
      tags 'Users'
      produces 'application/json'

      response '200', 'Users found' do
        before do
          create(:user)
          create(:user)
        end
        run_test!
      end
    end

    post 'Creates a user and runs a background job to link it to Hubspot' do
      tags 'Users'
      consumes 'application/json'
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              email: { type: :string }
            },
            required: ['email']
          }
        },
        required: ['user']
      }
      produces 'application/json'

      response '201', 'User created' do
        let(:params) { { user: { email: 'user@testdomain.com' } } }

        before do |example|
          VCR.use_cassette("integration/users/user_creation") do
            submit_request(example.metadata)
          end
        end

        it 'returns a valid 200 response' do |example|
          assert_response_matches_metadata(example.metadata)
        end
      end

      response '422', 'Invalid request' do
        let(:params) { { user: { email: Faker::Alphanumeric.alpha } } }
        run_test!
      end
    end
  end

  path '/api/v1/users/{id}.json' do
    get 'Retrieves a user' do
      tags 'Users'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'User found' do
        let(:user) { create(:user) }
        let(:id) { user.id }
        run_test!
      end

      response '404', 'User not found' do
        let(:id) { -1 }
        run_test!
      end
    end
  end

  path '/api/v1/users/{id}/link_to_hubspot.json' do
    post 'Manually links an existing user to Hubspot' do
      tags 'Users'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'user linked' do
        let(:user) { create(:user, email: 'user@customdomain.com') }
        let(:id) { user.id }

        before do |example|
          VCR.use_cassette("integration/users/link_to_hubspot") do
            submit_request(example.metadata)
          end
        end

        it 'returns a valid 200 response' do |example|
          assert_response_matches_metadata(example.metadata)
        end
      end

      response '404', 'User not found' do
        let(:id) { -1 }
        run_test!
      end

      response '422', 'Invalid request' do
        user = User.new(email: Faker::Alphanumeric.alpha)
        user.save(validate: false)

        let(:id) { user.id }

        run_test!
      end
    end
  end

  path '/api/v1/users/{id}/add_note.json' do
    post 'Adds a note on hubspot to a linked user' do
      tags 'Users'
      consumes 'application/json'

      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          note: { type: :string }
        },
        required: ['note']
      }

      produces 'application/json'

      response '201', 'Note Added' do
        
          let(:user) { create(:user, email: 'user@email.com') }
          let(:id) { user.id }
          let(:params) { { note: 'NEW_NOTE' } }

          before do |example|
            VCR.use_cassette("integration/users/add_note") do
              HubspotHandler::LinkUserService.new(user: user).call
              submit_request(example.metadata)
            end
          end

          it 'returns a valid 201 response' do |example|
            assert_response_matches_metadata(example.metadata)
          end
      end

      response '422', 'Invalid request' do
        let(:user) { create(:user) }
        let(:id) { user.id }
        let(:params) { {} }
        run_test!
      end
    end
  end
end
