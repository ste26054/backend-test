class AddHubspotAttributesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :hubspot_link_status, :integer, default: User.hubspot_link_statuses[:pending]
    add_column :users, :hubspot_id, :integer
  end
end
